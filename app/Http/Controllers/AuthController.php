<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function form() 
    {
        return view('pages.register');
    }

    public function welcome(Request $request)
    {
        //dd($request->all());
        $fname = $request->firstname;
        $lname = $request->lastname;

        return view('pages.welcome', compact('fname', 'lname'));
    }
}
