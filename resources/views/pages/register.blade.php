<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>SanberBook Sign Up</title>
</head>
<body>
    <form action="/welcome" method="post">
        @csrf
        <h1>Buat Account Baru!</h1>
        <h3>Sign Up Form</h3>
            <p>First name:</p>
            <input type="text" name="firstname">
            <p>Last name:</p>
            <input type="text" name="lastname">
            <p>Gender:</p>
            <input type="radio" name="gender">
            <label for="male">Male</label><br>
            <input type="radio" name="gender">
            <label for="female">Female</label><br>
            <input type="radio" name="gender">
            <label for="other">Other</label><br>
            <p>Nationality:</p>
            <select name="nationality" type="button" >
                <option value="I">Indonesian</option>
                <option value="S">Singaporean</option>
                <option value="M">Malaysian</option>
                <option value="A">Australian</option>
            </select>
            <p>Language Spoken:</p>
            <input type="checkbox"> Bahasa Indonesia <br>
            <input type="checkbox"> English <br>
            <input type="checkbox"> Other
            <p>Bio:</p>
            <textarea name="bio" cols="30" rows="10"></textarea><br>
            <button type="submit">Sign Up</button>
    </form>   
</body>

</html>